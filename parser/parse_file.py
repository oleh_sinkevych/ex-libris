from .pdf_parser import PdfParser
from .doc_parser import DocParser
from .docx_parser import DocxParser
import argparse
import pathlib


def factory(parser_type):
    if parser_type == "pdf":
        return PdfParser()
    if parser_type == "docx":
        return DocxParser()
    if parser_type == "doc":
        return DocParser()
    assert 0, "Bad parser creation: " + parser_type


ap = argparse.ArgumentParser()
ap.add_argument("-i", "--path_to_file", required=True, help="path to input file")
ap.add_argument("-f", "--file_to_write", required=True, help="path to output file")
args = vars(ap.parse_args())

# path_to_read = "/home/spirit/Projects/Ex-Libris/ex-libris/test/progit.pdf"
parser = factory(parser_type='pdf')
path_to_read = pathlib.Path(args["path_to_file"]).absolute()
print(path_to_read)
parser.read(path_to_read)
print(str(pathlib.Path(args["path_to_file"]).absolute().with_suffix('')) + '.txt')
path_to_write = str(pathlib.Path(args["path_to_file"]).absolute().with_suffix('')) + '.txt'
parser.write_txt(path_to_write)


# example how to run
# from top livel (ex-libris dir)
# python -m parser.parse_file --path_to_file test/progit.pdf --file_to_write test/git_book.txt
