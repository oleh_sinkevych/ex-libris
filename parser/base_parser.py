from abc import ABC, abstractmethod


class Parser(ABC):
    @abstractmethod
    def read(self, path_to_file):
        pass

    @abstractmethod
    def write_txt(self, path_to_write):
        pass
