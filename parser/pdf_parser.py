import fitz
import pathlib
from .base_parser import Parser


class PdfParser(Parser):

    def __init__(self):
        self._path = ''
        self._content = []

    def read(self, path_to_file):
        g_path = pathlib.Path(path_to_file)
        print(g_path.absolute())
        assert g_path.absolute().exists(), 'File is not found'
        pdf_file = fitz.open(path_to_file)
        num_of_pages = pdf_file.pageCount
        for page_num in range(num_of_pages):
            self._content.append(pdf_file.getPageText(page_num, output='text'))

    def write_txt(self, path_to_write):
        for page in self._content:
            with open(path_to_write, 'a') as (f):
                f.write(page)
